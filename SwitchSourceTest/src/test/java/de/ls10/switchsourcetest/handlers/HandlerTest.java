package de.ls10.switchsourcetest.handlers;

import static org.junit.Assert.*;

import org.junit.Test;

public class HandlerTest {

	@Test
	public void testGetTestType(){
		String testFile = Handler.getAssociatedFile("Main.java");
		assertEquals("MainTest.java",testFile);
	}
	
	@Test
	public void testBla(){
		String srcTypesFileName = Handler.getAssociatedFile("MainTest.java");
		assertEquals("Main.java",srcTypesFileName);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNotJavaFile(){
		Handler.getAssociatedFile("Foo.ja");
	}

}
