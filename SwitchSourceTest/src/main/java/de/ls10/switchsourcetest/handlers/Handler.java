package de.ls10.switchsourcetest.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

public class Handler extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		try {
			final String currentFile = getFilenNameOfCurrentClass();
			final String associatedFile = getAssociatedFile(currentFile);
			searchForFileAndOpenInEditor(associatedFile);
		} catch (CoreException e) {
			throw new ExecutionException("Something went wront: "+e.getMessage(), e);
		}
		return null;
	}

	/**
	 * returns the file name of the file which is active in current editor.
	 * @return
	 */
	private String getFilenNameOfCurrentClass() {
		IEditorPart activeEditor = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		return activeEditor.getTitle();
	}
	
	/**
	 * returns whether given java file name represents a test case, i. e. the
	 * type name has suffix "Test".
	 * 
	 * @param file
	 *            file name
	 * @return
	 */
	private static boolean isTestFile(String file) {
		return file.endsWith("Test.java");
	}

	/**
	 * returns whether given file name has the file extension ".java"
	 * 
	 * @param fileName
	 * @return
	 */
	private static boolean isJavaFileName(String fileName) {
		return fileName.endsWith(".java");
	}

	/**
	 * @param file
	 * @return
	 * @throws IllegalArgumentException
	 *             if file is not a java file, i.e. if it doesn' t end with
	 *             ".java".
	 */
	public static String getAssociatedFile(final String file)
			throws IllegalArgumentException {
		if (isJavaFileName(file)) {
			if (isTestFile(file)) {
				return file.replace("Test.java", ".java");
			} else {
				return file.replace(".java", "Test.java");
			}
		} else {
			throw new IllegalArgumentException(file+ " is not a java source file.");
		}
	}

	/**
	 * opens a given resource in a new editor view
	 * @param resource
	 * @throws PartInitException
	 */
	private void openInEditor(IResource resource) throws PartInitException {
		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();
		IWorkbenchPage page = window.getActivePage();
		IDE.openEditor(page, (IFile) resource);
	}

	/**
	 * searches all projects for given java file and opens it if search was successful.
	 * TODO we should only search for the file in the current project.
	 * @param javafile given java file
	 * @throws CoreException
	 */
	private void searchForFileAndOpenInEditor(String javafile)
			throws CoreException {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		for (IProject project : root.getProjects()) {
			if (project.isNatureEnabled("org.eclipse.jdt.core.javanature")) {
				IPackageFragment[] packages = JavaCore.create(project)
						.getPackageFragments();
				for (IPackageFragment mypackage : packages) {
					if (mypackage.getKind() == IPackageFragmentRoot.K_SOURCE) {
						for (ICompilationUnit unit : mypackage
								.getCompilationUnits()) {
							IResource resource = unit.getResource();
							if (resource.getName().equals(javafile)) {
								openInEditor(resource);

							}
						}
					}
				}
			}
		}
	}
}
